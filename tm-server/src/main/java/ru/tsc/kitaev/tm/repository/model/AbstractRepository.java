package ru.tsc.kitaev.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.api.repository.model.IRepository;

import javax.persistence.EntityManager;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository implements IRepository {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

}
