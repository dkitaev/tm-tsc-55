package ru.tsc.kitaev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractOwnerEntityDTO extends AbstractEntityDTO {

    @NotNull
    @Column(name = "user_id")
    protected String userId;

}
