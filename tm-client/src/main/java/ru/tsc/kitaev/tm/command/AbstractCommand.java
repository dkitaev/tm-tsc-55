package ru.tsc.kitaev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.api.service.ICommandService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.ISessionService;
import ru.tsc.kitaev.tm.endpoint.*;

@Component
public abstract class AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISessionService sessionService;

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected ProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    @Override
    public String toString() {
        String result = "";
        @Nullable String name = name();
        @Nullable String description = description();
        if (name != null) result += name + " ";
        if (description != null) result += ":" + description;
        return result;
    }

}
