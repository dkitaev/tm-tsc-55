package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name...";
    }

    @Override
    public void execute() {
        System.out.println("Enter Name");
        @NotNull final String name = TerminalUtil.nextLine();
        projectEndpoint.removeProjectByName(sessionService.getSession(), name);
        projectTaskEndpoint.removeByName(sessionService.getSession(), name);
        System.out.println("[OK]");
    }

}
