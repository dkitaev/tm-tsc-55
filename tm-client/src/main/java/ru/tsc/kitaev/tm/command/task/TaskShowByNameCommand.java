package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.endpoint.TaskDTO;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskShowByNameCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-show-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name...";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = taskEndpoint.findTaskByName(sessionService.getSession(), name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
