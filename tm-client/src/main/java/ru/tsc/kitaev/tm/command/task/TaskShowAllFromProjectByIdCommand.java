package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.endpoint.TaskDTO;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowAllFromProjectByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "tasks-show-by-project-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by project id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final List<TaskDTO> taskUpdated = projectTaskEndpoint.findTaskByProjectId(sessionService.getSession(), projectId);
        if (taskUpdated == null) throw new TaskNotFoundException();
        System.out.println(taskUpdated);
    }

}
