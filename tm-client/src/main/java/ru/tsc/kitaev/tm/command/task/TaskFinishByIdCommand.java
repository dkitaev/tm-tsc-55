package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskFinishByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        taskEndpoint.finishTaskById(sessionService.getSession(), id);
    }

}
